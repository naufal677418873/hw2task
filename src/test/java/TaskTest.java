import org.junit.Test;

import static org.junit.Assert.*;

public class TaskTest {
    @Test
    public void testTaskFunction(){
        String expectedReturn = "1. Do dishes [DONE]\n";
        Task task = new Task();
        //task.taskReturn(1,"Do dishes","DONE");

        assertEquals(expectedReturn,task.addTask(1,"Do dishes","DONE"));
    }
    @Test
    public void testtoDoList(){
        String expectedReturn = "1. Do dishes [DONE]\n2. Learn Java [NOT DONE]\n3. Learn TDD [NOT DONE]\n";
        Task task = new Task();

        task.addTask(1,"Do dishes","DONE");
        task.addTask(2,"Learn Java","NOT DONE");
        task.addTask(3,"Learn TDD","NOT DONE");

        assertEquals(expectedReturn,task.toDoList());
    }
}